# Weiterführende Anleitungen
## Schreiben mit Visual Studio Code
Man kann seine Arbeit auch bequem mit VS Code schreiben, statt mit Texmaker. Ob VS Code oder Texmaker besser zum Schreiben der Arbeit geeignet ist, muss jeder selbst wissen.

1. VS Code installieren: https://code.visualstudio.com/Download
2. Die VS Code Erweiterung "[LaTeX Workshop](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop)" installieren.
3. Die LaTex-Vorlage öffnen: "Datei -> Ordner öffnen..."
4. Sobald man eine .tex-Datei in der Vorlage ändert und speichert, wird ein Build-Vorgang gestartet.

Für die Rechtschreibprüfung in VS Code gibt es einige Add-ons. Hier werden zwei empfehlenswerte Add-ons vorgestellt:
1. "[LTeX+](https://marketplace.visualstudio.com/items?itemName=ltex-plus.vscode-ltex-plus)" prüft auf Grammatik und Rechtschreibung. In der Datei .vscode/settings.json sind bereits einige empfohlene Einstellungen für LTeX vorkonfiguriert. 
Wenn man Sätze auf Englisch schreibt, kann man für diese die Rechtschreibprüfung auf Englisch umschalten. Hier ist ein Beispiel:
```
\begin{english}
  This is an English text to check the spelling correction.
\end{english}
```

2. "[Spell Right](https://marketplace.visualstudio.com/items?itemName=ban.spellright)" ist eine Alternative zu LTeX prüft nur auf Rechtschreibfehler, aber nicht auf Grammatikfehler. In der Datei .vscode/settings.json sind bereits einige empfohlene Einstellungen für Spell Right vorkonfiguriert. Unter anderem ist eingestellt, dass Spell Right Deutsch und Englisch prüft, da die meisten Arbeiten viele englische Begriffe enthalten. Damit Spell Right unter Windows korrekt funktioniert, muss daher Englisch und Deutsch unter Windows als Sprachen installiert sein. Wie das geht, hat Microsoft hier beschrieben: https://support.microsoft.com/de-de/windows/sprachpakete-für-windows-a5094319-a92d-18de-5b53-1cfc697cfca8 

## Hinzufügen von weiteren Quellenarten
Standardmäßig gibt es im Quellenverzeichnis die Abschnitte Bücher, Sammelwerke, Artikel, Internetquellen, Interviews und interne Quellen. Man kann weitere Quellenarten hinzufügen.
Um die Anleitung etwas anschaulicher zu machen, wird als Beispiel wissenschaftliche Arbeiten als neue Quellenart angelegt.

1. Den passenden BIBTEX Database Entry Type wählen. <br> http://tug.ctan.org/info/biblatex-cheatsheet/biblatex-cheatsheet.pdf<br>
Für wissenschaftliche Arbeiten ist der Typ ```@thesis``` am besten geeignet. Falls kein Typ so richtig passt, kann man ```@misc``` nehmen.
2. In der Vorlage die Datei "text/references.tex" öffnen und folgenden Code hinzufügen:
```
\defbibheading{Arbeiten}{\noindent\large\textbf{Wissenschaftliche Arbeiten}} 
\printbibliography[type=thesis, heading=Arbeiten]
```
Mit dem Parameter "type" wird der Typ definiert, den wir im ersten Schritt gewählt haben.
3. Einen Eintrag in die Datei "text/content/bibliography.bib" hinzufügen, z. B. so:
```
@thesis{Meier2005,
  author       = {Linus Meier}, 
  title        = {LaTex in wissenschaftlichen Arbeiten},
  school       = {DHBW Lörrach},
  year         = 2005,
} 
```
4. Wenn der Eintrag zitiert wird, erscheint im Quellenverzeichnis der Abschnitt "Wissenschaftliche Arbeiten".

## Linux
Unter Linux muss man statt MiKTeX eine andere TeX-Distribution, z. B. TeX Live, installieren. Mit folgendem Befehl wird TeX Live installiert:
```
sudo apt-get install texlive-bibtex-extra biber texlive-lang-german texlive-latex-extra fonts-crosextra-carlito texlive-luatex texinfo
```
Danke @Zoidbart für diesen Beitrag (Issue #1)

